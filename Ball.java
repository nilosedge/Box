import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

	/**
	* THis is the ball class used for creating balls that have a mouse listener
	* attached to them. 
	*/
public class Ball extends JComponent {

	/**
	* This is the X value for the ball.
	*/
	private int ballx;
	/**
	* This is the Y value for the ball.
	*/
	private int bally;
	/**
	* This is the X off set for where the mouse clicks.
	*/
	private int offsetx;
	/**
	* This is the X off set for where the mouse clicks.
	*/
	private int offsety;
	/**
	* This is the width value for the ball.
	*/
	private int w;
	/**
	* This is the height value for the ball.
	*/
	private int h;
	/**
	* This is the bounding box width value for the ball.
	*/
	private int boxwidth;
	/**
	* This is the bounding box height value for the ball.
	*/
	private int boxheight;
	/**
	* This is the Ball Color 
	*/
	private Color ball_color;
	/**
	* These are the two different arrows for the region of the ball.
	*/
	Cursor arrow1,arrow3;

	/**
	* inx and iny are the X and Y for where the ball is going to start relitive to the 
	* bounding box, rad is the radious, ball_color is the color to set the ball, boxw
	* and boxh are the width and height of the bounding box that the ball is going to
	* move inside of. 
	*/
	public Ball(int inx, int iny, int rad, Color ball_color, int boxw, int boxh) {
		ballx = inx;
		bally = iny;
		w = h = rad * 2;
		setBounds(ballx, bally,w,h);
		this.ball_color = ball_color;
		arrow1 = getCursor();
		arrow3 = new Cursor(Cursor.MOVE_CURSOR);
		boxwidth = boxw;
		boxheight = boxh;

      addMouseListener( new MouseAdapter() {
         public void mouseEntered (MouseEvent e) { setCursor(arrow3); }
         public void mouseExited (MouseEvent e) { setCursor(arrow1); }
         public void mousePressed (MouseEvent e) {
				offsetx = e.getX();
				offsety = e.getY();
         }
      });

      addMouseMotionListener(new MouseMotionAdapter() {
         public void mouseDragged (MouseEvent e) {
				ballx = e.getX() - offsetx + ballx;
				bally = e.getY() - offsety + bally;
				if(ballx < 1) ballx = 1;
				if(bally < 1) bally = 1;
				if(ballx + w > boxwidth - 1) ballx = boxwidth - w - 1;
				if(bally + h > boxheight - 1) bally = boxheight - h - 1;
      		setBounds(ballx,bally,w,h);
         }
      });
	}

	/**
	* Actually paints the box to the screen for people to see it. 
	*/
   public void paint(Graphics g) {
		g.setColor(ball_color);
		g.fillOval(ballx,bally,w,h);
   }
}
