import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
*	This program is meant to be an application. Basicly it makes 4 move
*  boxes on the screen that can be moved around
*/
public class Box {
	/**
	*	This program does not take any args so there is no need to pass anything to is.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame("Composite Applet");
		frame.addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent e) { System.exit(0); } });
		frame.setSize(600, 600);
		frame.getContentPane().setBackground(Color.white);
		frame.getLayeredPane().add(new MoveBox(50,50,100,50,Color.red, 5,5,10,Color.yellow));
		frame.getLayeredPane().add(new MoveBox(150,150,200,100,Color.green, 30,30,30,Color.blue));
		frame.getLayeredPane().add(new MoveBox(250,250,300,100,Color.magenta, 50,50,30,Color.pink));
		frame.getLayeredPane().add(new MoveBox(150,250,300,300,Color.blue, 50,50,50,Color.red));
		frame.setVisible(true);
	}
}
