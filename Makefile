build: Box.java Ball.java MoveBox.java
	javac Box.java

all: build javadoc

clean:
	rm -fr *.class
	rm -fr javadoc_html

run: clean build
	java Box

javadoc:
	rm -fr javadoc_html
	mkdir javadoc_html
	javadoc -d javadoc_html *.java
