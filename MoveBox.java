import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
*	This is the class that is used to make a movable box on the screen.
*/
public class MoveBox extends Container {

/**
*	This is the X location of the box 
*/
	private int x;
/**
*	This is the Y location of the box.
*/
	private int y;
/**
*	This is the X offset for where the mouse clicks in the box 
*/
	private int offsetx;
/**
*	This is the Y offset for where the mouse clicks in the box 
*/
	private int offsety;
/**
*	This is the width of the box 
*/
	private int width;
/**
*	This is the height of the box 
*/
	private int height;
/**
*  This is the box color.
*/
	private Color box_color;
/**
*	These are the two crosshairs for the regions of the box.
*/
	Cursor arrow1,arrow2;
/**
*	This is the a ball1 that is added to the box 
*/
	Ball fooball;
/**
*	This is the a ball2 that is added to the box 
*/
	Ball fooball2;
/**
*	Move box is a movable box on the screen that has 2 balls inside of it that are movable also.
*  inx is the starting x positions for the box.
*  iny is the starting y positions for the box.
*  inwidth is the width for the box.
*  inheight is the height for the box.
*  inboxcolor is the color for the box.
*  ballx will be the x starting point for the balls inside of the box.
*  bally will be the y starting point for the balls inside of the box.
*  rad will be the radious given to the balls in the box.
*  ball_color will be the color of the balls in the box.
*/
	public MoveBox(int inx, int iny, int inwidth, int inheight, Color inbox_color, int ballx, int bally, int rad, Color ball_color) {
		x = inx;
		y = iny;
		width = inwidth;
		height = inheight;
		setBounds(x, y, width, height);
		box_color = inbox_color;

		fooball2 = new Ball(ballx+(rad/2), bally+(rad/2), rad/2, ball_color, inwidth, inheight);
		add(fooball2);
		fooball = new Ball(ballx, bally, rad, Color.black, inwidth, inheight);
		add(fooball);
		arrow1 = getCursor();
      arrow2 = new Cursor(Cursor.CROSSHAIR_CURSOR);

      addMouseListener( new MouseAdapter() {
         public void mouseEntered (MouseEvent e) { setCursor(arrow2); }
         public void mouseExited (MouseEvent e) { setCursor(arrow1); }
         public void mousePressed (MouseEvent e) {
            offsetx = e.getX();
            offsety = e.getY();
         }
      });

      addMouseMotionListener(new MouseMotionAdapter() {
         public void mouseDragged (MouseEvent e) {
            x = e.getX() - offsetx + x;
            y = e.getY() - offsety + y;
            setBounds(x, y, width, height);
         }
      });

	}
	/**
	* Method to actully draw the boxes and the balls on the screen.
	*/

   public void paint(Graphics g) {
		g.setColor(box_color);
		g.fillRect(0,0,width,height);
		g.setColor(Color.black);
		g.drawRect(0,0,width-1,height-1);
		fooball.paint(g);
		fooball2.paint(g);
   }

}
